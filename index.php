<!DOCTYPE html>
<html>
<head>
	<title>Home</title>

	<link rel="stylesheet" type="text/css" href="index.css">
	<?php
	include('header.php');
	?>
</head>
<body>
	<br><br><br><br>
	<div id="slide_imgs" style="box-shadow: 5px 5px 5px #BCF5A9,-2px -2px 1px #BCF5A9" >
	

			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" >
		  <ol class="carousel-indicators">
		    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active" ></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="1" ></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="2" ></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="3" ></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
		  </ol>
		<div class="carousel-inner" style="border-radius: 10px;width: 85%; position: relative;left: 7.5%;box-shadow: 5px 5px 10px black;">
			<div class="carousel-item ">
		      <img class="d-block w-100" src="img1\slide_1.png" alt="First slide">
		    </div>
		    <div class="carousel-item">
		      <img class="d-block w-100" src="img1\slide_2.jpg" alt="Second slide">
		    </div>
		    <div class="carousel-item">
		      <img class="d-block w-100" src="img1\slide_3.jpg" alt=" Third slide">
		    </div>
		    <div class="carousel-item ">
		      <img class="d-block w-100" src="img1\slide_4.jpg" alt="fourth slide">
		    </div>
		    <div class="carousel-item">
		      <img class="d-block w-100" src="img1\slide_5.jpg" alt="fifth slide">
		    </div>
		    <div class="carousel-item active">
		      <img class="d-block w-100" src="img1\slide_6.jpg" alt="sixth slide">
		    </div>
		</div>
		  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</div>
<br><br><br>
<div id="main_body">
                        <span class="main_body">
                            <p id="descr">		

                                <span id="pam" align="justify">

                                    <img class="img-responsive" src="http://rgvcbsedwd.ac.in/design/names/welcome.png" width="80%"><br><br>
                                    <div style="width: 70%;position: relative;left: 2%;font-size: 23px;font-family: Rockwell;color: #0A2A0A;">
                                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                    Learning through activities and interaction are the most suitable methods for young learners. To develop interest towards learning is the main purpose of any educational institution. Our school has been started with an intention to provide the best schooling experience to the children of Dharwad.
                                    <br><br>
                                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                    We are striving hard to develop the all-round personality of children and thereby develop good human beings and the best citizens for the future.
                                    <br><br>
                                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                    We work for the preservation of our rich Indian culture and tradition by inculcating the spirit of nationality and human values in the younger generation. 
                                    </div>
                                </span><br><br><br>
                                <a href="http://rgvcbsedwd.ac.in/design/images/home.jpg" rel="prettyPhoto[gallery1]">
                                    <img class="img-responsive" id="pimg1" src="http://rgvcbsedwd.ac.in/design/images/home.jpg" style="position: relative;left: 20%;box-shadow: 5px 5px 10px black;border-radius: 10px;"></a>

                                <br><br><br><br>
                            </p>
                        </span>
                    </div>
	<div id="main_body" style="background-color: #BCF5A9;" >
		<div style="width: 100%;">
                    <img class="img-responsive" src="http://rgvcbsedwd.ac.in/design/names/name2.png" width="80%"><br><br>
                    <span class="main_body" >
                        

                        <div class="col-sm-12 col-xs-12">
                            <div class="col-sm-4"></div>
                            
                            <div class="col-sm-4">
                                <img class="img-responsive" src="http://rgvcbsedwd.ac.in/design/images/president.png" style="float: left;">
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <!--<div id="man_data" >-->
                                <div>
                                    <h4 style=" color: #7e160b; ">NADOJA JAUSTICE DR. V.S.MALIMATH</h4>
	                                    <h4>PRESIDENT</h4>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>


                        
                        <div class="col-sm-12 col-xs-12" style="position: relative;left: 50%">
                            <div class="col-sm-4"></div>
                            
                            <div class="col-sm-4" >
                                <img class="img-responsive" src="http://rgvcbsedwd.ac.in/design/images/wp.png" style="float: left;">
                            </div>
                            <div class="col-sm-4 col-xs-12" >
                                <!--<div id="man_data" >-->
                                <div>
                                    <h4 style=" color: #7e160b;">SHRI R.F.NIRALKATTI</h4>
                                    <h4>WORKING PRESIDENT</h4>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        
                        <div class="col-sm-12 col-xs-12">
                            <div class="col-sm-4">
                                <img class="img-responsive" src="http://rgvcbsedwd.ac.in/design/images/tre.png" style="float: left;">
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <!--<div id="man_data" >-->
                                <div><br>
                                    <h4 style=" color: #7e160b;">V.S.GANGADHARMATH</h4>
                                    <h4>TREASURER</h4>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                       
                        <div class="col-sm-12 col-xs-12" style="position: relative;left: 50%">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4 col-xs-12">
                                <img class="img-responsive" src="http://rgvcbsedwd.ac.in/design/images/sec1.png" style="float: left;">
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <!--<div id="man_data" >-->
                                <div>
                                    <h4 style=" color: #7e160b;">PANDURANGA RAO T.S.V.</h4>
                                    <h4>SECRETARY</h4>
                                </div>
                            </div>
                            
                            <div style="clear: both;"></div>
                        </div>
                       
                        <div class="col-sm-12 col-xs-12">
                            <div class="col-sm-4">
                                <img class="img-responsive" src="http://rgvcbsedwd.ac.in/design/images/sec2.png" style="float: left;">
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <!--<div id="man_data" >-->
                                <div>
                                    <h4 style=" color: #7e160b;">VIJAYAN.K.</h4>
                                    <h4>SECRETARY</h4>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                         <br><br>
                        

                  
                        
                    </span>

                    <p></p>
                    
                   
                

                   <br><br><br>
                </div>

	</div>

</body>
</html>
<footer>
	<?php
		include('footer.php');
	?>
</footer>