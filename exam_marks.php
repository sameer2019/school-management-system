<!DOCTYPE html>
<html>
<head>
	<title>Exam Marks</title>
</head>

<style type="text/css">
	#back2{
		position: relative;
		left: 0%;
	}
	#t_b{
		font-size: 27px;
		font-family: elephant;
		color: #FFFF00;
		text-shadow: 3px 3px 4px black;
	}
	#btn{

		width: 60%;
		background-color: #2E2E2E;
		color: white;
		padding: 7px;
		font-weight:bold;
		border-radius: 50px;
		box-shadow: 3px 3px 3px white,-3px -3px 3px white;
	}
	#btn:hover{

		background-color: #A4A4A4;
		color: black;
		box-shadow: 3px 3px 3px black,-3px -3px 3px black;
	}
	td{
		padding: 20px;
	}
	
</style>

<body style="background-image:url('img1/b3.jpg');">

	<?php
				session_start();

				if($_SESSION['t_id']=="")
				{
					header('location:index.php');
				}

				include('header_log.php');

				include('db.php');


				$t_id = $_SESSION['t_id'];

				$sql = "SELECT class,division FROM teacher WHERE t_id = $t_id";
				$r = mysqli_query($db,$sql);
				$row = mysqli_fetch_array($r);
	?>


	<div id="back2">
		<br><br><br>
		<center>
			<h2 style="color: #070719;text-shadow: 2px 2px 2px white;">Class : <?php echo $row['class']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Division : <?php echo $row['division']; ?></h3><br><br>
		</center>
		
		
		
<center>
	

		<table border="0" width="70%" style="text-align: center;">
			<tr id="t_b">
				<td>
					<span>Formative Assessment</span> 
				</td>
				<td>
					<span>Summative Assessment</span>
					
				</td>
			</tr>
			<tr>
				<td>
					<a href="fa1.php"><button id="btn">Formative Assessment - I</button></a>
				</td>
				<td>
					<a href="sa1.php"><button id="btn">Summative Assessment - I</button></a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="fa2.php"><button id="btn">Formative Assessment - II</button></a>
				</td>
				<td>
					<a href="sa2.php"><button id="btn">Summative Assessment - II</button></a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="fa3.php"><button id="btn">Formative Assessment - III</button></a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="fa4.php"><button id="btn">Formative Assessment - IV</button></a>
				</td>
			</tr>
		</table>
</center>
		<br><br>
			<center>	
					
					<a href="teacher_home.php"><button style="width: 10%; background-color: #100719;color:white;border-radius: 10px; box-shadow: 3px 3px 5px black,-3px -3px 5px black; font-weight: bolder; padding: 5px;"  onMouseOver="this.style.background='#8181F7';this.style.color='black';" onMouseOut="this.style.background='#100719';this.style.color='white';">Back</button></a>

			</center>

	</div>

</body>
</html>

<br>
<br><br><br>
<?php

	include('footer.php');

?>