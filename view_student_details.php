<!DOCTYPE html>
<html>
<head>
	<title>
		View Student Details
	</title>
</head>

<style type="text/css">
	
	#bar{

		font-size: 17px;
		text-align: center;
		font-weight: bold;
		font-family: cooper black;
		height: 50px;
		text-shadow: 1px 1px 3px white;

	}

	#rows{

		text-align: center;
		font-size: 17px;
		font-family: century Gothic;
		color: black	;
		background-color: #E6E0F8;
		height: 40px;
		font-weight: bold;
	}

</style>

<body style="background-image: url('img1/i_back.jpg')">
	<?php

		session_start();

		if($_SESSION['admin_id'] == "")
		{
			header('location:index.php');
		}

		include('db.php');

		include('header_log.php');


		$result = mysqli_query($db,"SELECT * FROM student ORDER BY reg_no") or mysql_error();

	?>

	<br>
	<br>
	<br>
	<br>
	<table border="3" width="95%" style="box-shadow: 5px 5px 10px black;position: relative;left: 2%;" >
		<tr bgcolor="#F3F781" id="bar">
				<td>Register No.</td>
				<td>First Name</td>
				<td>Middle Name</td>
				<td>Last Name</td>
				<td>Father Name</td>
				<td>Mother Name</td>
				<td>Date of birth</td>
				<td>Address</td>
				<td></td>
				<td></td>

			</tr>
			<?php
				while($row = mysqli_fetch_array($result))
				{
					$id = $row['reg_no'];

			?>			
			<tr id="rows">
				<td><?php echo $row['reg_no'] ?></td>
				<td><?php echo $row['fname'] ?></td>
				<td><?php echo $row['mname'] ?></td>
				<td><?php echo $row['lname'] ?></td>
				<td><?php echo $row['father_name'] ?></td>
				<td><?php echo $row['mother_name'] ?></td>
				<td><?php echo date('d-m-Y', strtotime( $row['dob'] )) ?></td>
				<td><?php echo $row['address'] ?></td>
				<td>
					<?php echo "<a href='edit_student_form.php?id=$id'  style='font-size:20px;color:blue;'>&nbsp;&nbsp;Edit&nbsp;&nbsp; </a>" ?>
				</td>
				<td>
					<?php echo "<a href='delete_student_form.php?id=$id'  style='font-size:20px;color:red;'>&nbsp;&nbsp;Delete&nbsp;&nbsp; </a>" ?>
				</td>
			</tr>
			<?php
				}

			?>
		</table>

		<br><br><br>
		<center>
			<a href="admin_home.php"><button style="width: 7%; background-color: #100719;color:white;border-radius: 10px; box-shadow: 2px 2px 5px black;font-weight: bold;"  onMouseOver="this.style.background='#81BEF7';this.style.color='black';" onMouseOut="this.style.background='#100719';this.style.color='white';">Back</button></a>

	</center>

</body>
</html>


<br><br><br><br><br>
<?php 
	include('footer.php');
?>