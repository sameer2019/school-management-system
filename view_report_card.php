<!DOCTYPE html>
<html>
<head>
	<title>Report Card</title>
</head>
<body>

	<?php
	session_start();

	if($_SESSION['id']=="")
		{
			header('location:index.php');
		}

	include('db.php');

	$reg = $_SESSION['id'];


	$sql = mysqli_query($db,"SELECT * FROM student WHERE reg_no = $reg ") or mysql_error();
	$row = mysqli_fetch_array($sql);
	$cl = $row['class'];

	$ch = mysqli_query($db,"SELECT * FROM report_card WHERE class = $cl  ") or mysql_error();
	$row1 = mysqli_fetch_array($ch);


	if(($row1['value'])==1)
	{


	?>
	<center>
		<div>
			<table border="0" style="width: 70%; border-top: 2px solid black;border-left:2px solid black;border-right: 2px solid black; background:#F7F8E0;">
				<tr>
					<td colspan="7">
						<img src="img1/school_logo.png" style="width: 100%;height: 100px;box-shadow: 2px 0px 5px black;" id="" >
					</td>
					
				</tr>
				<tr>
					<td style="background-color: #5E610B;font-size: 5px;" colspan="7">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;Name </td><td>:</td>
					<td><b><?php echo strtoupper($row['fname']." ".$row['mname']." ".$row['lname']) ?></b></td>
					<td>&nbsp;</td>
					<td>Register No. &nbsp;</td><td>:</td>
					<td style="font-size: 20px;"><b>&nbsp;&nbsp;<?php echo $row['reg_no'] ?>&nbsp;&nbsp;</b></td>
				</tr> 
				<tr>
					<td>&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;Father Name &nbsp;&nbsp;</td><td>:</td>
					<td><b><?php echo strtoupper($row['father_name']) ?></b></td>
					<td></td>
					
				</tr>
				<tr>
					<td>&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;Mother Name &nbsp;&nbsp;</td><td>:</td>
					<td><b><?php echo strtoupper($row['mother_name']) ?></b></td>

				</tr>
				<tr>
					<td>&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;Date Of Birth&nbsp;</td><td>:</td>
					<td><b><?php echo date('d-m-Y', strtotime( $row['dob'] )) ?></b></td>

				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				
				<tr>
					<td>&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;Class &nbsp;</td><td>:</td>
					<td><b><?php  echo $row['class'] ?></b></td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;Division &nbsp;</td><td>:</td>
					<td><b><?php  echo $row['division'] ?></b></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>

			</table>
			
			<table border="0" style="width: 70%;text-align: center;border-bottom: 2px solid black;border-left:2px solid black;border-right: 2px solid black; background:#F7F8E0;">
				<tr>
					<td>------------------------</td>
					<td>-</td>
					<td>---------</td>
					<td>---------</td>
					<td>---------</td>
					<td>----------</td>
					<td>----------</td>
					<td>----------</td>
				</tr>
				<tr>

					<td><b>SUBJECTS</b></td>
					<td>|</td>
					<td>
						<b>&nbsp;&nbsp;FA I &nbsp;&nbsp;</b>
					</td>
					<td><b>&nbsp;&nbsp;FA II&nbsp;&nbsp;</b></td>
					<td><b>&nbsp;&nbsp;SA I&nbsp;&nbsp;</b></td>
					<td><b>&nbsp;&nbsp;FA III&nbsp;&nbsp;</b></td>
					<td><b>&nbsp;&nbsp;FA IV&nbsp;&nbsp;</b></td>
					<td><b>&nbsp;&nbsp;SA II&nbsp;&nbsp;</b></td>
				</tr>
				<tr>
					<td>------------------------</td>
					<td>-</td>
					<td>---------</td>
					<td>---------</td>
					<td>---------</td>
					<td>----------</td>
					<td>----------</td>
					<td>----------</td>
				</tr>
				<tr>
					<td>ENGLISH</td>
					<td>|</td>
					<td><?php echo $row['fa1_e'] ?></td>
					<td><?php echo $row['fa2_e'] ?></td>
					<td><?php echo $row['sa1_e'] ?></td>
					<td><?php echo $row['fa3_e'] ?></td>
					<td><?php echo $row['fa4_e'] ?></td>
					<td><?php echo $row['sa2_e'] ?></td>
				</tr>
				<tr>
					<td>------------------------</td>
					<td>-</td>
					<td>---------</td>
					<td>---------</td>
					<td>---------</td>
					<td>----------</td>
					<td>----------</td>
					<td>----------</td>
				</tr>
				<tr>
					<td>HINDI</td>
					<td>|</td>
					<td><?php echo $row['fa1_h'] ?></td>
					<td><?php echo $row['fa2_h'] ?></td>
					<td><?php echo $row['sa1_h'] ?></td>
					<td><?php echo $row['fa3_h'] ?></td>
					<td><?php echo $row['fa4_h'] ?></td>
					<td><?php echo $row['sa2_h'] ?></td>
				</tr>
				<tr>
					<td>------------------------</td>
					<td>-</td>
					<td>---------</td>
					<td>---------</td>
					<td>---------</td>
					<td>----------</td>
					<td>----------</td>
					<td>----------</td>
				</tr>
				<tr>
					<td>KANNADA</td>
					<td>|</td>
					<td><?php echo $row['fa1_k'] ?></td>
					<td><?php echo $row['fa2_k'] ?></td>
					<td><?php echo $row['sa1_k'] ?></td>
					<td><?php echo $row['fa3_k'] ?></td>
					<td><?php echo $row['fa4_k'] ?></td>
					<td><?php echo $row['sa2_k'] ?></td>
				</tr>
				<tr>
					<td>------------------------</td>
					<td>-</td>
					<td>---------</td>
					<td>---------</td>
					<td>---------</td>
					<td>----------</td>
					<td>----------</td>
					<td>----------</td>
				</tr>
				<tr>
					<td>MATHEMATICS</td>
					<td>|</td>
					<td><?php echo $row['fa1_m'] ?></td>
					<td><?php echo $row['fa2_m'] ?></td>
					<td><?php echo $row['sa1_m'] ?></td>
					<td><?php echo $row['fa3_m'] ?></td>
					<td><?php echo $row['fa4_m'] ?></td>
					<td><?php echo $row['sa2_m'] ?></td>
				</tr>
				<tr>
					<td>------------------------</td>
					<td>-</td>
					<td>---------</td>
					<td>---------</td>
					<td>---------</td>
					<td>----------</td>
					<td>----------</td>
					<td>----------</td>
				</tr>
				<tr>
					<td>SCIENCE</td>
					<td>|</td>
					<td><?php echo $row['fa1_sc'] ?></td>
					<td><?php echo $row['fa2_sc'] ?></td>
					<td><?php echo $row['sa1_sc'] ?></td>
					<td><?php echo $row['fa3_sc'] ?></td>
					<td><?php echo $row['fa4_sc'] ?></td>
					<td><?php echo $row['sa2_sc'] ?></td>
				</tr>
				<tr>
					<td>------------------------</td>
					<td>-</td>
					<td>---------</td>
					<td>---------</td>
					<td>---------</td>
					<td>----------</td>
					<td>----------</td>
					<td>----------</td>
				</tr>
				<tr>
					<td>SOCIAL SCIENCE</td>
					<td>|</td>
					<td><?php echo $row['fa1_ss'] ?></td>
					<td><?php echo $row['fa2_ss'] ?></td>
					<td><?php echo $row['sa1_ss'] ?></td>
					<td><?php echo $row['fa3_ss'] ?></td>
					<td><?php echo $row['fa4_ss'] ?></td>
					<td><?php echo $row['sa2_ss'] ?></td>
				</tr>
				<tr>
					<td>------------------------</td>
					<td>-</td>
					<td>---------</td>
					<td>---------</td>
					<td>---------</td>
					<td>----------</td>
					<td>----------</td>
					<td>----------</td>
				</tr>
				<tr>
					<td>COMPUTER</td>
					<td>|</td>
					<td><?php echo $row['fa1_c'] ?></td>
					<td><?php echo $row['fa2_c'] ?></td>
					<td><?php echo $row['sa1_c'] ?></td>
					<td><?php echo $row['fa3_c'] ?></td>
					<td><?php echo $row['fa4_c'] ?></td>
					<td><?php echo $row['sa2_c'] ?></td>
				</tr>
				<tr>
					<td>------------------------</td>
					<td>-</td>
					<td>---------</td>
					<td>---------</td>
					<td>---------</td>
					<td>----------</td>
					<td>----------</td>
					<td>----------</td>
				</tr>
				<tr>
					<td>PERCENTAGE</td>
					<td>|</td>
					<td><b><?php echo round($row['fa1_per'],2) ?></b></td>
					<td><b><?php echo round($row['fa2_per'],2) ?></b></td>
					<td><b><?php echo round($row['sa1_per'],2) ?></b></td>
					<td><b><?php echo round($row['fa3_per'],2) ?></b></td>
					<td><b><?php echo round($row['fa4_per'],2) ?></b></td>
					<td><b><?php echo round($row['sa2_per'],2) ?></b></td>
				</tr>
				<tr>
					<td>------------------------</td>
					<td>-</td>
					<td>---------</td>
					<td>---------</td>
					<td>---------</td>
					<td>----------</td>
					<td>----------</td>
					<td>----------</td>
				</tr>

			</table>
		</div>
	</center>

	<?php

	}
	else
	{
		echo "hello";

	}

	?>

</body>
	<br><br><br>
	<button style="position: relative;left: 45%; background-color: skyblue;padding: 10px;width: 5%;height: 30px;font-weight: bold" onclick="window.print()" >Print</button>
	<br><br><br>

</html>