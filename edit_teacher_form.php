<!DOCTYPE html>
<html>
<head>
	<title>
		Teacher form
	</title>
</head>

<style type="text/css">
	
	#back2{

		width: 60%;
	/*	background-color: white;*/
		position: relative;
		left: 20%;
	}

	#ff{

		font-size: 19px;
		font-weight: bold;
		font-family: lucida bright;
		color: #E0ECF8;
		/*text-shadow: 3px 3px 20px black;*/

	}
	#in{

		text-align: center;
		box-shadow: 3px 3px 2px black;
		width: 25%;
		padding: 3px;
		border-radius: 6px;
		background-color: white;
		color:#0A0A2A;
		font-size: 20px;
		font-family: times new roman;
		font-weight: bold;
	}

	#s_b{

		background-color: #819FF7;
		font-weight: bolder;
		color: black;
		padding: 7px;

	}
	#s_b:hover{

		color:#0A2A0A;
		box-shadow: 1px 1px 15px white;
	}
	span{

		font-size: 19px;
		font-weight: bold;
		font-family: lucida bright;
		color: #E0ECF8;
		text-shadow: 3px 3px 20px black;
	}


</style>

<body style="background:linear-gradient(#220A29,#220A29,#0B0B61)">


<?php 

	session_start();

	if($_SESSION['admin_id'] == "")
	{
		header('location:index.php');
	}

		if(!isset($_POST['submit']))
		{

			include('db.php');

			include('header_log.php');

				
			$id=$_GET['id'];

			$_SESSION['t_id'] = $_GET['id'];	

			$res = mysqli_query($db,"SELECT * FROM teacher WHERE t_id = '$id' ");
			
			$row = mysqli_fetch_array($res);

			


?>

<br><br><br><br><br>
	<div id="back2">
		<!-- <form method="POST" action="<?php $_SERVER['PHP_SELF']  ?>">
			<input type="text" name="firstname" placeholder="First Name" value="<?php echo $row['fname']; ?>" required> &nbsp;&nbsp;
			<input type="text" name="middlename" placeholder="Middle Name" value="<?php echo $row['mname']; ?>" required> &nbsp;&nbsp;
			<input type="text" name="lastname" placeholder="Last Name" value="<?php echo $row['lname']; ?>" required> &nbsp;&nbsp;<br><br>
			<input type="text" name="fathername" placeholder="Father Name" value="<?php echo $row['father_name']; ?>" required ><br><br>
			<input type="text" name="mothername" placeholder="Mother Name" value="<?php echo $row['mother_name']; ?>" required ><br><br>
			<table border="1">
				<tr><td>
					<label><u>Select Subjects</u></label><br>
					<input type="checkbox" name="subject" value="english"> English <br>
					<input type="checkbox" name="subject" value="Hindi"> Hindi <br>
					<input type="checkbox" name="subject" value="kannada"> Kannada <br>
					<input type="checkbox" name="subject" value="mathematics"> Mathematics <br>
					<input type="checkbox" name="subject" value="socialscience"> Social Science <br>
					<input type="checkbox" name="subject" value="science"> Science <br>
					<input type="checkbox" name="subject" value="computer"> Computer <br>
				</td></tr>
			</table><br><br>
			<input type="number" name="bed_marks" placeholder="B.Ed. Marks(%)" value="<?php echo $row['bed_marks']; ?>" required><br><br>
			<input type="number" name="degree_marks" placeholder="Degree Marks(%)" value="<?php echo $row['degree_marks']; ?>" required><br><br>
			<label>Job Type :</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="jobtype" value="guest">Guest &nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="jobtype" value="fulltime">Fulltime<br><br>
			<input type="date" name="dob" value="<?php echo $row['dob']; ?>" required><br><br>
			<input type="text" name="address" value="<?php echo $row['address']; ?>" placeholder="address"><br><br>
			<input type="text" name="qualification" value="<?php echo $row['qualification']; ?>" placeholder="Highest Qualification"><br><br>
			<input type="number" name="class" value="<?php echo $row['class']; ?>" placeholder="Class"><br><br>
			<input type="text" name="division" value="<?php echo $row['division']; ?>" placeholder="Division"><br><br>
			<input type="submit" name="submit" value="submit">

		</form>

 -->


		<form method="POST" action="<?php $_SERVER['PHP_SELF']  ?>">
			<input id="in" type="text" name="firstname" value="<?php echo $row['fname']; ?>" placeholder="First Name" required> &nbsp;&nbsp;&nbsp;&nbsp;
			<input id="in" type="text" name="middlename" value="<?php echo $row['mname']; ?>" placeholder="Middle Name" required> &nbsp;&nbsp;&nbsp;&nbsp;
			<input id="in" type="text" name="lastname" value="<?php echo $row['lname']; ?>" placeholder="Last Name" required> &nbsp;&nbsp;<br><br>
			<input id="in" type="text" name="fathername" value="<?php echo $row['father_name']; ?>" placeholder="Father Name" required ><br><br>
			<input id="in" type="text" name="mothername" value="<?php echo $row['mother_name']; ?>" placeholder="Mother Name" required ><br><br><br>
			<table border="0" width="30%">
				<tr><td>
					<label id="ff"><u> <span>Select Subject</span> </u></label><br>
					<input id="ff" type="checkbox" name="subject" value="english"><span>&nbsp;English</span>  <br>
					<input id="ff" type="checkbox" name="subject" value="Hindi"> <span>&nbsp;Hindi</span>  <br>
					<input id="ff" type="checkbox" name="subject" value="kannada"><span>&nbsp;Kannada</span>  <br>
					<input id="ff" type="checkbox" name="subject" value="mathematics"><span>&nbsp;Mathematics</span>  <br>
					<input id="ff" type="checkbox" name="subject" value="socialscience"> <span>&nbsp;Social Science</span>  <br>
					<input id="ff" type="checkbox" name="subject" value="science"> <span>&nbsp;Science</span>  <br>
					<input id="ff" type="checkbox" name="subject" value="computer"><span>&nbsp;Computer</span>  <br>
				</td></tr>
			</table><br><br>
			<input id="in" type="number" name="bed_marks" placeholder="B.Ed. Marks(%)" value="<?php echo $row['bed_marks']; ?>"  required><br><br>
			<input id="in" type="number" name="degree_marks" placeholder="Degree Marks(%)"  value="<?php echo $row['degree_marks']; ?>" required><br><br>
			<label ><span>Job Type :</span> </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="jobtype" value="guest"><span style="font-size: 16px;"> &nbsp;Guest</span>  &nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="jobtype" value="fulltime"><span style="font-size: 16px;">&nbsp;Fulltime</span> <br><br>
			<input id="in" type="date" name="dob" value="<?php echo $row['dob']; ?>" required><br><br>
			<input id="in" type="text" name="address" value="<?php echo $row['address']; ?>" placeholder="Address"><br><br>
			<input id="in" type="text" name="qualification" value="<?php echo $row['qualification']; ?>" placeholder="Highest Qualification"><br><br>
			<input id="in" type="number" name="class" value="<?php echo $row['class']; ?>" placeholder="Class"><br><br>
			<input id="in" type="text" name="division" value="<?php echo $row['division']; ?>" placeholder="Division"><br><br><br><br>

			<input type="submit" name="submit" value="Submit" id="s_b" style="position: relative;left: 40%;width: 20%; border-radius: 5px; font-size: 18px;">

		</form>
		<br><br><br><br>
		<center>
				<a href="view_teacher_form.php"><button style="width: 9%; background-color: #100719;color:white;border-radius: 50px; box-shadow: 2px 2px 5px black; font-weight: bold;"  onMouseOver="this.style.background='#8181F7';this.style.color='black';" onMouseOut="this.style.background='#100719';this.style.color='white';">Back</button></a>
				</center>




	</div>

</body>
<?php
		
	
	}
	else{

		include('db.php');

		
		$id = $_SESSION['t_id'];

		$fname = $_POST['firstname'];
		$mname = $_POST['middlename'];
		$lname = $_POST['lastname'];
		$fathername = $_POST['fathername'];
		$mothername = $_POST['mothername'];
		$subject = $_POST['subject'];
		$bed_marks = $_POST['bed_marks'];
		$degree_marks = $_POST['degree_marks'];
		$job_type = $_POST['jobtype'];
		$dob = $_POST['dob'];
		$address = $_POST['address'];
		$qualification = $_POST['qualification'];
		$class = $_POST['class'];
		$division = $_POST['division'];

		$sql =  "UPDATE teacher SET fname='$fname',mname = '$mname',lname = '$lname',dob ='$dob', father_name = '$fathername',mother_name =   '$mothername',address = '$address',subject = '$subject', class = $class, division = '$division',job_type = '$job_type',bed_marks = $bed_marks, degree_marks = $degree_marks, qualification = '$qualification'  WHERE t_id = '$id'";

		$result = mysqli_query($db,$sql) or mysql_error();
		if ($result){
			
				// echo "$id";
			?>
			<div>
				<?php include('header_log.php'); ?>
						<center>
							
							<br>
							<img src="img1/submit.png" width="5%">
							<br>
								<br>
								<h1 style='position: relative;top: -10px;text-align: center;font-size: 22px; color: yellow;'>
									Teacher &nbsp; Details&nbsp;  Updated &nbsp; Successfully... <br> _______________________________</h2>
								<br>
								<a href="view_teacher_form.php"><button style="width: 7%; background-color: #100719;color:white;border-radius: 10px; box-shadow: 2px 2px 5px black; font-weight: bold;"  onMouseOver="this.style.background='#8181F7';this.style.color='black';" onMouseOut="this.style.background='#100719';this.style.color='white';">Back</button></a>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="admin_home.php"><button style="width: 10%; background-color: #100719;color:white;border-radius: 10px; box-shadow: 2px 2px 5px black; font-weight: bold;"  onMouseOver="this.style.background='#8181F7';this.style.color='black';" onMouseOut="this.style.background='#100719';this.style.color='white';">Back To Home</button></a>
							</center>
					</div>
		
			<?php
		}
	}

 ?>


</html>