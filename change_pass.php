<!DOCTYPE html>
<html>
<head>
	<title>Change Password</title>
</head>
<style type="text/css">
	
	input{

		font-weight: bold;
		text-align: center;
		box-shadow: 3px 3px 10px black;
		border-radius: 5px;
		padding: 5px;
	}
	#sb{

		width: 8%;
		background-color: #0B0B3B;
		color: white;
		border-radius: 15px;
		font-family: lucida console;
	}

	#sb:hover{
		background-color:white ;
		color: #0B0B3B;

	}

</style>
<body style="background:linear-gradient(#BDBDBD,#1C1C1C);">

	<?php
		include('db.php');
		include('header_log.php');

		session_start();

		if(!isset($_POST['submit']))
		{


	?>
	<br><br>
	<center>
		<div>
		<form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
			<input type="number" name="Reg_no" placeholder="Admin Id" required><br><br>
				<input type="Password" name="old_pass" id="old_pass" placeholder="Enter Old Password" required><br><br>
			<input type="Password" name="new_pass" id="new_pass" placeholder="Enter New Password" onblur="old_match()" required><br><br>
			<input type="Password" name="confirm_pass" id="confirm_pass" placeholder="Confirm Password" onblur="confirm_pass()" required><br><br>
			<input type="submit" id="sb" name="submit" value="Update">

				
		</form>

		<br><br><br>
			<a href="parent_login.php"><button style="width: 5%; background-color: #100719;color:white;border-radius: 10px; box-shadow: 2px 2px 5px black; font-weight: bold;position: relative;left: 0%"  onMouseOver="this.style.background='#8181F7';this.style.color='black';" onMouseOut="this.style.background='#100719';this.style.color='white';"><<</button></a>
			</div>
			</center>
			

	
	<?php

		}
		else
		{

			$old_pass = $_POST['old_pass'];
			$new_pass = $_POST['new_pass'];

			$id  = $_POST['Reg_no'];

			$res = mysqli_query($db,"SELECT * FROM admin WHERE id = $id ");

			$row = mysqli_fetch_array($res);
			$check = $row['password'];


			if($check == $old_pass)
			{
				$sql = mysqli_query($db,"UPDATE admin SET password = '$new_pass' WHERE id = $id ");
				if($sql)
				{
					?>
					<center>
						<h3 style="color: #088A08;">Password has been Successfully Changed.</h3>
						<br>
						<a href="admin_login.php"><button style="width: 5%; background-color: #100719;color:white;border-radius: 10px; box-shadow: 2px 2px 5px black; font-weight: bold;position: relative;left: 0%"  onMouseOver="this.style.background='#8181F7';this.style.color='black';" onMouseOut="this.style.background='#100719';this.style.color='white';"><<</button></a>
					</center>
					
					<?php
				}
			}
			else
			{
				?>
					<center>
						<h2 style="color: #B40404;">Invalid register No. / Old Password</h2>
					<br>
					<a href="admin_login.php"><button style="width: 5%; background-color: #100719;color:white;border-radius: 10px; box-shadow: 2px 2px 5px black; font-weight: bold;position: relative;left: 0%"  onMouseOver="this.style.background='#8181F7';this.style.color='black';" onMouseOut="this.style.background='#100719';this.style.color='white';"><<</button></a>

					</center>
				<?php
			}


		}

	?>

</body>
</html>
<br><br><br><br>
<?php
	
	include('footer.php');

?>


<script type="text/javascript">
	function confirm_pass() {
		var pass1 = document.getElementById('new_pass').value;
		var pass2 = document.getElementById('confirm_pass').value;
		if(pass1!=pass2)
		{
			alert("please enter the same password")
		}

	}
	function old_match()
	{
		var old = document.getElementById('old_pass').value;
		var new1 = document.getElementById('new_pass').value;

		if(old==new1){
			alert("you can't enter your old password");
		}

	}
</script>